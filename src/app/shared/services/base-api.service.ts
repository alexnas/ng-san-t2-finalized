import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseApiService {
  // Service to provide BaseApi and main CRUD operations.

  constructor(private http: HttpClient) { }

  public getData = (route: string) => {
    return this.http.get(route, this.generateHeaders());
  }

  public postData = (route: string, body) => {
    return this.http.post(route, body, this.generateHeaders());
  }

  public update = (route: string, body) => {
    return this.http.put(route, body, this.generateHeaders());
  }

  public delete = (route: string) => {
    return this.http.delete(route, this.generateHeaders());
  }

  public getAuthHeader = () => {
    const token = localStorage.getItem('token');
    if (token) {
      return {'Authorization': `Bearer ${token}`};
    }
  };

  private generateHeaders = () => {
    const newHeaders = {
      headers: new HttpHeaders({ ...(this.getAuthHeader()), 'Content-Type': 'application/json' })
    }
    return newHeaders
  }
}
