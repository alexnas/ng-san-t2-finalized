import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class AuthService {

  // Set user and token from local storage
  setLoggedIn(token: string, user: any) {
    console.log("setLoggedIn", token, user);
    localStorage.setItem('token', token);
    localStorage.setItem('user', user);
  }

  // Remove user and token from local storage === clear localStorage
  setLoggedOut() {
    window.localStorage.clear();
  }

  // Token is found in localStorage and it is not expired  
  isLoggedIn(): boolean {
    const helper = new JwtHelperService();
    const curToken = localStorage.getItem('token')
    const isExpired = helper.isTokenExpired(curToken);
    return (curToken !== null) && !isExpired;
  }
}
