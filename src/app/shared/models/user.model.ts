export class User {
  constructor(
    public email: string,
    public password: string,
    public name: string,
    public description: string,
    public active: boolean,
    public created_at: string,
    public updated_at: string,
    public location_id: number,
    public group: number,
    public id?: number,

  ) { }
}
