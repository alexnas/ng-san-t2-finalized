export class Contact {
    constructor(
        public fname: string,
        public lname: string,
        public company: string,
        public phone: string,
        public passwd: string,
        public email: string,
        public status: boolean,
        public group: number,
        public notes: string,
        public created_at: string,
        public updated_at: string,
        public id?: number,
    ) { }
}
