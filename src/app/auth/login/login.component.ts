import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';
import { Subscription } from 'rxjs';

import { BaseApiService } from 'src/app/shared/services/base-api.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { User } from 'src/app/shared/models/user.model';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  sub1: Subscription;
  dataSource = new MatTableDataSource<User>();
  form: FormGroup;
  username: string;
  password: string;
  token_expires: Date;

  isExpired: boolean;

  constructor(
    private baseApiService: BaseApiService,
    private authService: AuthService,
    private http: HttpClient,
    private router: Router,
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      username: new FormControl(null, [Validators.required, Validators.minLength(4)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(4)])
    });
  }

  public login = (username: string, password: string) => {
    let body = JSON.stringify({ username, password });
    console.log('login', name)
    this.sub1 = this.baseApiService.postData("auth/signin", body)
      .subscribe(res => {
        console.log('res', res);
        this.authService.setLoggedIn(res["token"], res["user"]);
        this.router.navigate(["system/orders"]);
      })
  }

  onSubmit() {
    const formData = this.form.value;
    this.login(formData['username'], formData['password'])
  }

  ngOnDestroy() {
    if (this.sub1) this.sub1.unsubscribe();
  }

}
