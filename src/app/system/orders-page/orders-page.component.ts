import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Subscription } from 'rxjs';

import { BaseApiService } from 'src/app/shared/services/base-api.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Order } from 'src/app/shared/models/order.model';


@Component({
  selector: 'app-orders-page',
  templateUrl: './orders-page.component.html',
  styleUrls: ['./orders-page.component.scss']
})
export class OrdersPageComponent implements OnInit, OnDestroy {
  sub1: Subscription;

  isLoaded = false;
  dataSource = new MatTableDataSource<Order>();
  displayedColumns = ["invoice", "status", "currency", "created_at",
    "updated_at", "total_cost"];

    @ViewChild(MatPaginator, {'static': false}) paginator: MatPaginator;
  @ViewChild(MatSort, {'static': false}) sort: MatSort;

  constructor(
    private baseApiService: BaseApiService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    console.log('isAuthenticated - on init', this.authService.isLoggedIn());
    this.getAllUsers();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public getAllUsers = () => {
    this.sub1 = this.baseApiService.getData('api/orders')
      .subscribe(res => {
        console.log('res', res);
        this.dataSource.data = res as Order[];
        console.log('this.dataSource.data', this.dataSource.data);
      })
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  public isAuthenticated(): boolean {
    return localStorage.getItem('token') !== null;
  }

  ngOnDestroy() {
    if (this.sub1) {
      this.sub1.unsubscribe();
    }
  }
}
