import { AuthService } from 'src/app/shared/services/auth.service';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Subscription } from 'rxjs';

import { User } from 'src/app/shared/models/user.model';
import { BaseApiService } from 'src/app/shared/services/base-api.service';


@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit, OnDestroy {
  sub1: Subscription;

  isLoaded = false;
  dataSource = new MatTableDataSource<User>();
  displayedColumns = ["name", "email", "group", "created_at",  'details', 'update', 'delete'];

  @ViewChild(MatPaginator, {'static': false}) paginator: MatPaginator;
  @ViewChild(MatSort, {'static': false}) sort: MatSort;

  constructor(
    private baseApiService: BaseApiService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.getAllUsers();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public getAllUsers = () => {
    this.sub1 = this.baseApiService.getData('api/user')
      .subscribe(res => {
        console.log('res', res);
        this.dataSource.data = res as User[];
        console.log('this.dataSource.data', this.dataSource.data);
      })
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  isAuthenticated() {
    return this.authService.isLoggedIn();
  }

  ngOnDestroy() {
    if (this.sub1) {
      this.sub1.unsubscribe();
    }
  }
}
