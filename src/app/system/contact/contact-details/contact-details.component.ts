import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { BaseApiService } from 'src/app/shared/services/base-api.service';
import { Contact } from './../../../shared/models/contact_model';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit, OnDestroy {
  sub1: Subscription;
  form: FormGroup;

  contact: Contact;

  constructor(
    private baseApiService: BaseApiService,
    private router: Router,
    private activeRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.getContactDetails();
  }

  private getContactDetails = () => {
    let activeId: string = this.activeRoute.snapshot.params['id'];
    let apiUrl: string = `api/contacts/${activeId}`;


    this.sub1 = this.baseApiService.getData(apiUrl)
      .subscribe(res => {
        this.contact = res as Contact;
      },
        (error) => {
          console.log('getContactDetails error === ', error);
        })
  }

  onReturnToList() {
    let url: string = `system/contacts`;
    this.router.navigate([url]);
  }

  public onRedirectToUpdate = (id: string) => {
    let url: string = `system/contacts/${id}/update`;
    this.router.navigate([url]);
  }

  onDelete(contact: any) {
    if (confirm("Are you sure to delete contact with name " + contact.fname + " " + contact.lname)) {
      let activeId: string = this.activeRoute.snapshot.params['id'];
      let apiUrl: string = `api/contacts/${activeId}`;
      this.baseApiService.delete(apiUrl)
        .subscribe(res => {
          this.contact = res as Contact;
        },
          (error) => {
            console.log('getContactDetails error === ', error);
          })

      let url: string = `system/contacts`;
      this.router.navigate([url]);
    }
  }

  ngOnDestroy() {
    if (this.sub1) this.sub1.unsubscribe();
  }
}
