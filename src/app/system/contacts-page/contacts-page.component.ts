import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { BaseApiService } from 'src/app/shared/services/base-api.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Order } from 'src/app/shared/models/order.model';

@Component({
  selector: 'app-contacts-page',
  templateUrl: './contacts-page.component.html',
  styleUrls: ['./contacts-page.component.scss']
})
export class ContactsPageComponent implements OnInit {

  sub1: Subscription;

  isLoaded = false;
  dataSource = new MatTableDataSource<Order>();
  displayedColumns = ['fname', 'lname', 'company', 'phone', 'email', 'group',
    'details', 'update', 'delete'];

  @ViewChild(MatPaginator, { 'static': false }) paginator: MatPaginator;
  @ViewChild(MatSort, { 'static': false }) sort: MatSort;

  constructor(
    private baseApiService: BaseApiService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    console.log('isAuthenticated - on init', this.authService.isLoggedIn());
    this.getAllUsers();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public getAllUsers = () => {
    this.sub1 = this.baseApiService.getData('api/contacts')
      .subscribe(res => {
        console.log('res', res);
        this.dataSource.data = res as Order[];
        console.log('this.dataSource.data', this.dataSource.data);
      })
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  public isAuthenticated(): boolean {
    return localStorage.getItem('token') !== null;
  }

  public redirectToDetails = (id: string) => {
    let url: string = `system/contacts/${id}`;
    console.log('url', url);
    this.router.navigate([url]);
  }

  ngOnDestroy() {
    if (this.sub1) {
      this.sub1.unsubscribe();
    }
  }
}
