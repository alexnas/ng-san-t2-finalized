import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";

import { SystemComponent } from "./system.component";
import { UsersPageComponent } from "./users-page/users-page.component";
import { ContactsPageComponent } from './contacts-page/contacts-page.component';
import { ContactDetailsComponent } from './contact/contact-details/contact-details.component';
import { LocationsPageComponent } from './locations-page/locations-page.component';
import { InventeryPageComponent } from './inventery-page/inventery-page.component';
import { OrdersPageComponent } from './orders-page/orders-page.component';
import { ChannelsPageComponent } from './channels-page/channels-page.component';
import { ReportsPageComponent } from './reports-page/reports-page.component';

const routes: Routes = [
  {
    path: "",
    component: SystemComponent,
    children: [
      { path: "users", component: UsersPageComponent },
      { path: "contacts", component: ContactsPageComponent },
      { path: "contacts/:id", component: ContactDetailsComponent },
      { path: "locations", component: LocationsPageComponent },
      { path: "inventery", component: InventeryPageComponent },
      { path: "orders", component: OrdersPageComponent },
      { path: "channels", component: ChannelsPageComponent },
      { path: "reports", component: ReportsPageComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule { }
