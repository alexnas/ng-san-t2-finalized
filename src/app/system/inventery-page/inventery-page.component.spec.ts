import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventeryPageComponent } from './inventery-page.component';

describe('InventeryPageComponent', () => {
  let component: InventeryPageComponent;
  let fixture: ComponentFixture<InventeryPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventeryPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventeryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
