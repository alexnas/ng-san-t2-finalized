import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Material section
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule, } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { SystemRoutingModule } from "./system-routing.module";
import { SystemComponent } from "./system.component";
import { UsersPageComponent } from "./users-page/users-page.component";
import { ContactsPageComponent } from './contacts-page/contacts-page.component';
import { LocationsPageComponent } from './locations-page/locations-page.component';
import { InventeryPageComponent } from './inventery-page/inventery-page.component';
import { OrdersPageComponent } from './orders-page/orders-page.component';
import { ChannelsPageComponent } from './channels-page/channels-page.component';
import { ReportsPageComponent } from './reports-page/reports-page.component';
import { MomentPipe } from '../shared/pipes/moment.pipe';
import { BaseApiService } from '../shared/services/base-api.service';
import { ContactDetailsComponent } from './contact/contact-details/contact-details.component';


@NgModule({
  imports: [
    CommonModule,
    SystemRoutingModule,
    FlexLayoutModule,

    // Material
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    MatMenuModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule
  ],
  exports: [],
  declarations: [
    SystemComponent,
    UsersPageComponent,
    ContactsPageComponent,
    LocationsPageComponent,
    InventeryPageComponent,
    OrdersPageComponent,
    ChannelsPageComponent,
    ReportsPageComponent,
    MomentPipe,
    ContactDetailsComponent,
  ],
  providers: [BaseApiService]
})
export class SystemModule { }
