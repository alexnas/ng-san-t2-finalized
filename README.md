# CSD-a

Angular 8+ version of CSDashboard

## Development server

Run `yarn start --proxyConfig=proxy.conf.json` for a dev server. Change target endpoint urls inside the proxy config to your actual server host and port.

## Build

Run `yarn build` to build the project. The build artifacts will be stored in the `dist/csd-a` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
